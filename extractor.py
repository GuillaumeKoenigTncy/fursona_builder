from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

import time
from selenium.common.exceptions import NoSuchElementException

import undetected_chromedriver as uc


options = uc.ChromeOptions() 
options.headless = False 
driver = uc.Chrome(use_subprocess=True, options=options) 

driver.get("https://a-z-animals.com/animals/")

elems = driver.find_elements(by=By.XPATH, value="//a[starts-with(@href, 'https://a-z-animals.com/animals/')]")

links = []

animals = []
dogs = []
cats = []

for i, elem in enumerate(elems):
    links.append(str(elem.get_attribute("href")))

for i, elem in enumerate(links):
    driver.get(elem)

    try:
        if len(driver.find_elements(by=By.CLASS_NAME, value='animal-facts-box')) == 0:
            pass
        else:
            time.sleep(1)
            print(driver.find_element(by=By.XPATH, value="/html/body/div[2]/div[1]/div/main/article/div/div[1]/div/div[1]/div/h1").text)
            if driver.find_element(by=By.XPATH, value="/html/body/div[2]/div[1]/div/main/article/div/div[1]/div/div[1]/div/p").text.lower() == "canis lupus":
                print("  Dog")
                dogs.append(driver.find_element(by=By.XPATH, value="/html/body/div[2]/div[1]/div/main/article/div/div[1]/div/div[1]/div/h1").text)
            elif driver.find_element(by=By.XPATH, value="/html/body/div[2]/div[1]/div/main/article/div/div[1]/div/div[1]/div/p").text.lower() == "felis catus":
                print("  Cat")
                cats.append(driver.find_element(by=By.XPATH, value="/html/body/div[2]/div[1]/div/main/article/div/div[1]/div/div[1]/div/h1").text)
            else:
                print("  Other")
                animals.append(driver.find_element(by=By.XPATH, value="/html/body/div[2]/div[1]/div/main/article/div/div[1]/div/div[1]/div/h1").text)
    except NoSuchElementException:
        pass
        print("NO ELEM")
        # time.sleep(0.5)


with open(r'animals.txt', 'w') as fp:
    for item in animals:
        fp.write("%s\n" % item)

with open(r'dogs.txt', 'w') as fp:
    for item in dogs:
        fp.write("%s\n" % item)

with open(r'cats.txt', 'w') as fp:
    for item in cats:
        fp.write("%s\n" % item)
