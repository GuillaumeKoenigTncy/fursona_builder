# pylint: disable=import-error, line-too-long

import logging
import os
import random
import string

from os import listdir
from os.path import isfile, join

from flask import jsonify, make_response, render_template, request  # type: ignore
import spotipy                                                      # type: ignore
from spotipy.oauth2 import SpotifyClientCredentials                 # type: ignore

from app import app

from .data import bckg, colors, seasons, numbers_of_friends, relationship_status, weights, heights, skin_color, eyes_color

logger = logging.getLogger(__name__)

DEBUG = True if os.getenv("DEBUG") == "1" else False
URL_BASE = os.getenv("URL_BASE")

app.debug = DEBUG 
app.config['SECRET_KEY'] = os.getenv("SECRET_KEY")

with open("app/animals.txt", "r", encoding="utf-8") as my_file:
    data = my_file.read()
    animals = data.split("\n")

with open("app/dogs.txt", "r", encoding="utf-8") as my_file:
    data = my_file.read()
    dogs = data.split("\n")

with open("app/cats.txt", "r", encoding="utf-8") as my_file:
    data = my_file.read()
    cats = data.split("\n")

species = animals + cats + dogs

IMAGE_DIR = "app/static/images/"

genders = [f.replace(".png", "") for f in listdir(IMAGE_DIR + "genders") if isfile(join(IMAGE_DIR + "genders", f))]
sexualitys = [f.replace(".png", "") for f in listdir(IMAGE_DIR + "sexualitys") if isfile(join(IMAGE_DIR + "sexualitys", f))]
hobbies = [f.replace(".png", "") for f in listdir(IMAGE_DIR + "hobbies") if isfile(join(IMAGE_DIR + "hobbies", f))]
foods = [f.replace(".png", "") for f in listdir(IMAGE_DIR + "foods") if isfile(join(IMAGE_DIR + "foods", f))]
drinks = [f.replace(".png", "") for f in listdir(IMAGE_DIR + "drinks") if isfile(join(IMAGE_DIR + "drinks", f))]
dislikes = [f.replace(".png", "") for f in listdir(IMAGE_DIR + "dislikes") if isfile(join(IMAGE_DIR + "dislikes", f))]
sports = [f.replace(".png", "") for f in listdir(IMAGE_DIR + "sports") if isfile(join(IMAGE_DIR + "sports", f))]
pets = [f.replace(".png", "") for f in listdir(IMAGE_DIR + "pets") if isfile(join(IMAGE_DIR + "pets", f))]
likes_generals = [f.replace(".png", "") for f in listdir(IMAGE_DIR + "likes") if isfile(join(IMAGE_DIR + "likes", f))]
likes_friends = [f.replace(".png", "") for f in listdir(IMAGE_DIR + "likes/friends") if isfile(join(IMAGE_DIR + "likes/friends", f))]
likes_smells = [f.replace(".png", "") for f in listdir(IMAGE_DIR + "likes/smells") if isfile(join(IMAGE_DIR + "likes/smells", f))]
wheres = [f.replace(".png", "") for f in listdir(IMAGE_DIR + "wheres") if isfile(join(IMAGE_DIR + "wheres", f))]
whens = [f.replace(".png", "") for f in listdir(IMAGE_DIR + "whens") if isfile(join(IMAGE_DIR + "whens", f))]

client_credentials_manager = SpotifyClientCredentials(client_id="4559a6464a444231a79886c047328556", client_secret="914f432dff6844c3b89d686c527afa01")
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

stories = [
    "I always lost my keys !",
    "I open my bananas backwards",
    "My laugh is ridiculous",
    ""
]

@app.route('/')
def home():

    # First Block
    fursona_species_major = random.choice(species)
    fursona_species_minor = random.sample(species, random.randint(0, 2))
    if not len(fursona_species_minor) == 0:
        while len([value for value in fursona_species_minor if value in fursona_species_major]) > 0:
            fursona_species_minor = random.sample(species, random.randint(1, 2))
    fursona_gender = [random.choice(genders)]
    if fursona_gender[0] == "trans":
        fursona_gender.append(random.choice(["male", "female"]))
    fursona_height = random.choice(heights)
    fursona_weight = random.choice(weights)

    # Second Block
    fursona_color_major = random.choice(colors)
    fursona_color_minor = random.sample(colors, random.randint(1, 3))
    while len([value for value in fursona_color_minor if value in fursona_color_major]) > 0:
        fursona_color_minor = random.sample(colors, random.randint(1, 3))

    fursona_eyes_color = random.choice(eyes_color)
    fursona_skin_color = random.choice(skin_color)

    # Third Block
    fursona_food = random.choice(foods)
    fursona_drink = random.choice(drinks)
    fursona_season = random.choice(seasons)
    fursona_sport = random.choice(sports)
    fursona_hobbies = random.sample(hobbies, random.randint(1, 4))
    fursona_likes = random.sample(likes_generals, random.randint(2, 4))
    fursona_likes_smell = random.choice(likes_smells)
    fursona_dislikes = random.sample(dislikes, random.randint(1, 7 - len(fursona_likes)))

    # Fourth Block
    fursona_sexuality = random.choice(sexualitys)
    fursona_pet = random.choice(pets)
    fursona_relationship_status = random.choice(relationship_status)
    fursona_likes_friends = random.choice(likes_friends)
    fursona_number_of_friends = random.choice(numbers_of_friends)

    fursona_where = random.choice(wheres)
    fursona_when = random.choice(whens)

    genres = ["blues", "metal", "rock", "pop", "french", "techno", "rock-n-roll", "anime", "classical", "electro"]

    fursona_music_genre = "Not realy into specific music genre !"
    track_id = False

    while track_id is None:
        try:

            track_random = sp.search(q=''.join(random.choices(string.ascii_letters + string.digits, k=3)) + "and genre:" + random.choice(genres), type='track',limit=1, offset=random.randint(0,100))

            # artist_name = track_random["tracks"]["items"][0]["album"]["artists"][0]["name"]
            # album_image = track_random["tracks"]["items"][0]["album"]["images"][2]["url"]
            # spotify_url = track_random["tracks"]["items"][0]["external_urls"]["spotify"]
            # track_name = track_random["tracks"]["items"][0]["name"]

            track_id = track_random["tracks"]["items"][0]["id"]

            artist = sp.artist(track_random["tracks"]["items"][0]["album"]["artists"][0]["external_urls"]["spotify"])
            for elem in genres:
                for elemz in artist["genres"]:
                    if elem in elemz:
                        fursona_music_genre = random.choice(["Prefers ", "Like ", "Really love ", "Fan of "]) + elem + " music"
                        break

        except Exception as e:
            logger.error(e)
            track_id = None

    # logger.error(fursona_music_genre)
    # logger.error(track_id)

    my_fursona = {
        "fursona_species_major" : fursona_species_major,
        "fursona_species_minor" : fursona_species_minor,
        "fursona_gender" : fursona_gender,
        "fursona_height": fursona_height,
        "fursona_weight": fursona_weight,

        "fursona_color_major" : fursona_color_major,
        "fursona_color_minor" : fursona_color_minor,
        "fursona_eyes_color": fursona_eyes_color,
        "fursona_skin_color": fursona_skin_color,

        "fursona_food" : fursona_food,
        "fursona_hobbies" : fursona_hobbies,
        "fursona_likes" : fursona_likes,
        "fursona_likes_smell" : fursona_likes_smell,
        "fursona_likes_friends" : fursona_likes_friends,
        "fursona_dislikes" : fursona_dislikes,
        "fursona_season" : fursona_season,
        "fursona_drink" : fursona_drink,
        "fursona_sport" : fursona_sport,

        "fursona_pet" : fursona_pet,
        "fursona_sexuality" : fursona_sexuality,
        "fursona_number_of_friends" : fursona_number_of_friends,
        "fursona_relationship_status" : fursona_relationship_status,
        "fursona_music_genre" : fursona_music_genre,

        "fursona_where" : fursona_where,
        "fursona_when" : fursona_when,
    }

    return render_template('home.html', page="home", my_fursona=my_fursona, random_season=random.randint(1, 4), random_logo=random.randint(1, 9), random_bckg=random.choice(bckg), track_id=track_id)

@app.route('/reroll')
def reroll():
    data = {}
    args = request.args
    if "colormajor" in args["reroll"]:
        data = {'colormajor': random.choice(colors).capitalize()}
    elif "colorminor" in args["reroll"]:
        data = {'colorminor': random.choice(colors).capitalize()}
    elif "coloreyes" in args["reroll"]:
        data = {'coloreyes': random.choice(eyes_color)}
    elif "colorskin" in args["reroll"]:
        data = {'colorskin': random.choice(skin_color)}
    elif "major" in args["reroll"]:
        data = {'specmajor': random.choice(species).capitalize()}
    elif "specminor" in args["reroll"]:
        data = {'specminor': random.choice(species).capitalize()}
    elif "weight" in args["reroll"]:
        data = {'weight': random.choice(weights).capitalize()}
    elif "height" in args["reroll"]:
        data = {'height': random.choice(heights).capitalize()}
    elif "gender" in args["reroll"]:
        fursona_gender = [random.choice(genders).capitalize()]
        if fursona_gender[0] == "Trans":
            fursona_gender.append(random.choice(["male", "female"]))
        data = {'gender': fursona_gender}
    elif "sexuality" in args["reroll"]:
        data = {'sexuality': random.choice(sexualitys).capitalize()}
    elif "relationship" in args["reroll"]:
        data = {'relationship': random.choice(relationship_status)}
    elif "food" in args["reroll"]:
        data = {'food': random.choice(foods).capitalize()}
    elif "drink" in args["reroll"]:
        data = {'drink': random.choice(drinks).capitalize()}
    elif "season" in args["reroll"]:
        data = {'season': random.choice(seasons).capitalize()}
    elif "sport" in args["reroll"]:
        data = {'sport': random.choice(sports).capitalize()}
    elif "pet" in args["reroll"]:
        data = {'pet': random.choice(pets).capitalize()}
    elif "hobbies" in args["reroll"]:
        data = {'hobbies': random.sample(hobbies, random.randint(1, 4))}
    elif "dislikes" in args["reroll"]:
        data = {'dislikes': random.sample(dislikes, random.randint(2, 4))}
    elif "likes" in args["reroll"]:
        data = {'likes': random.sample(likes_generals, random.randint(2, 4)) + [random.choice(likes_smells)]}
    elif "where" in args["reroll"]:
        data = {'where': random.choice(wheres).capitalize()}
    elif "when" in args["reroll"]:
        data = {'when': random.choice(whens).capitalize()}

    return jsonify(data)


@app.route('/statistics')
def statistics():
    details = None
    requested = None

    args = request.args
    if len(args) > 0:
        requested = args["detail"]
        details = sorted(eval(args["detail"]))

    colors_statistics = {
        "colors" : len(colors),
        "skin_color" : len(skin_color),
        "eyes_color" : len(eyes_color)
    }
    colors_statistics = dict(sorted(colors_statistics.items()))

    nopics_statistics = {
        "numbers_of_friends" : len(numbers_of_friends),
        "relationship_status" : len(relationship_status),
        "weights" : len(weights),
        "heights" : len(heights),
    }
    nopics_statistics = dict(sorted(nopics_statistics.items()))

    pics_statistics = {
        "seasons" : len(seasons),
        "genders" : len(genders),
        "sexualitys" : len(sexualitys),
        "hobbies" : len(hobbies),
        "foods" : len(foods),
        "drinks" : len(drinks),
        "dislikes" : len(dislikes),
        "sports" : len(sports),
        "pets" : len(pets),
        "likes_generals" : len(likes_generals),
        "likes_friends" : len(likes_friends),
        "likes_smells" : len(likes_smells),
        "wheres" : len(wheres),
        "whens" : len(whens), 
    }
    pics_statistics = dict(sorted(pics_statistics.items()))

    return render_template('statistics.html', page="statistics", random_logo=1, random_bckg="BDCDD6", colors_statistics=colors_statistics, nopics_statistics=nopics_statistics, pics_statistics=pics_statistics, details=details, requested=requested)


@app.route('/faq')
def faq():
    return render_template('faq.html', page="faq", random_logo=1, random_bckg="BDCDD6")


@app.route('/legal')
def legal():
    return render_template('legal.html', page="legal", random_logo=1, random_bckg="BDCDD6")


@app.route('/changelog')
def changelog():
    return render_template('changelog.html', page="changelog", random_logo=1, random_bckg="BDCDD6")


@app.route('/sitemap.xml', methods=['GET'])
def sitemap():
    response = make_response(open('./app/sitemap.xml').read())
    response.headers["Content-type"] = "text/plain"
    return response


@app.route('/robots.txt', methods=['GET'])
@app.route('/robot.txt', methods=['GET'])
def robot():
    response = make_response(open('./app/robots.txt').read())
    response.headers["Content-type"] = "text/plain"
    return response


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', random_logo=1, random_bckg="BDCDD6"), 404


@app.errorhandler(500)
def server_error(e):
    return render_template('500.html', random_logo=1, random_bckg="BDCDD6"), 500


@app.errorhandler(403)
def not_authorized(e):
    return render_template('403.html', random_logo=1, random_bckg="BDCDD6"), 403
