Affenpinscher
Afghan Hound
Aidi
Ainu
Airedale Terrier
Airedoodle
Akbash
Akita
Akita Shepherd
Alabai
Alaskan Husky
Alaskan Klee Kai
Alaskan Malamute
Alaskan Shepherd
Alpine Dachsbracke
Alusky
American Alsatian
American Bulldog
American Bully
American Cocker Spaniel
American Coonhound
American Eskimo Dog
American Foxhound
American Hairless Terrier
American Leopard Hound
American Pit Bull Terrier
American Pugabull
American Staffordshire Terrier
American Water Spaniel
Anatolian Shepherd Dog
Appenzeller Dog
Apple Head Chihuahua
Armenian Gampr
Aussiedoodle
Aussiedor
Australian Bulldog
Australian Cattle Dog
Australian Kelpie Dog
Australian Retriever
Australian Shepherd
Australian Shepherd Mix
Australian Terrier
Barbet
Basenji Dog
Bassador
Basset Fauve de Bretagne
Basset Hound
Bassetoodle
Bavarian Mountain Hound
Beagador
Beagle
Beagle Mix
Beagle Shepherd
Beaglier
Beago
Bearded Collie
Beaski
Beauceron
Bedlington Terrier
Belgian Laekenois
Belgian Malinois Mix
Belgian Shepherd
Belgian Tervuren
Bergamasco
Berger Picard
Bernese Mountain Dog Mix
Bernese Shepherd
Bichon Frise
Bichpoo
Black Mouth Cur
Black Russian Terrier
Bloodhound
Blue German Shepherd
Blue Lacy Dog
Blue Picardy Spaniel
Bluetick Coonhound
Boerboel
Boglen Terrier
Bolognese Dog
Borador
Border Collie
Border Collie Mix
Border Terrier
Borkie
Boston Terrier
Bouvier Des Flandres
Boxador
Boxer Dog
Boxer Mix
Boxsky
Boxweiler
Boykin Spaniel
Bracco Italiano
Braque Francais
Brazilian Terrier
Briard
British Timber
Brittany
Brug
Brussels Griffon
Bull and Terrier
Bull Terrier
Bulldog
Bulldog Mix
Bullmastiff
Cairn Terrier
Canaan Dog
Canadian Eskimo Dog
Cane Corso
Carolina Dog
Catahoula Leopard
Catalan Sheepdog
Cava Tzu
Cavador
Cavalier King Charles Spaniel
Cesky Fousek
Cesky Terrier
Cheagle
Chesapeake Bay Retriever
Chi Chi
Chihuahua
Chinese Crested Dog
Chinese Shar-Pei
Chinook
Chipit
Chipoo
Chiweenie
Chorkie
Chow Chow
Chow Shepherd
Chusky
Clumber Spaniel
Cockalier
Cocker Spaniel
Collie
Comfort Retriever
Corgidor
Corkie
Corman Shepherd
Coton de Tulear
Curly Coated Retriever
Czechoslovakian Wolfdog
Dachsador
Dachshund
Dachshund Mix
Dalmadoodle
Dalmador
Dalmatian
Dandie Dinmont Terrier
Dapple Dachshund
Daug
Deer Head Chihuahua
Deutsche Bracke
Doberman Pinscher
Dogo Argentino
Dogue De Bordeaux
Dorkie
Double Doodle
Drever
Dunker
Dutch Shepherd
East Siberian Laika
English Bulldog
English Cocker Spaniel
English Foxhound
English Pointer
English Setter
English Shepherd
English Springer Spaniel
Entlebucher Mountain Dog
Epagneul Pont Audemer
Eskimo Dog
Eskipoo
Estrela Mountain Dog
Feist
Field Spaniel
Fila Brasileiro
Finnish Lapphund
Finnish Spitz
Flat-Coated Retriever
Formosan Mountain Dog
French Bulldog Mix
Frenchton
Frengle
Frug
Gerberian Shepsky
German Longhaired Pointer
German Pinscher
German Shepherd
German Shepherd Mix
German Sheppit
German Sheprador
German Shorthaired Pointer
German Spitz
Giant Schnauzer
Glechon
Glen Of Imaal Terrier
Goberian
Goldador
Golden Dox
Golden Irish
Golden Newfie
Golden Pyrenees
Golden Retriever
Golden Retriever Mix
Golden Saint
Golden Shepherd
Goldendoodle
Gollie
Gordon Setter
Great Dane
Great Danoodle
Great Pyrenees
Great Pyrenees Mix
Greater Swiss Mountain Dog
Greenland Dog
Greyhound
Griffonshire
Groenendael
Harrier
Havamalt
Havanese
Havashire
Havashu
Hokkaido
Horgi
Huntaway
Huskador
Huskita
Husky
Husky Jack
Huskydoodle
Icelandic Sheepdog
Irish Terrier
Irish Water Spaniel
Irish WolfHound
Italian Greyhound
Jack Russell
Jackabee
Jagdterrier
Japanese Chin
Japanese Terrier
Kai Ken
Keagle
Kerry Blue Terrier
King Shepherd
Kishu
Komondor
Kooikerhondje
Koolie
Korean Jindo
Kuvasz
Labahoula
Labmaraner
Labrabull
Labradane
Labradoodle
Labrador Retriever
Labraheeler
Lagotto Romagnolo
Lakeland Terrier
Lancashire Heeler
Landseer Newfoundland
Lapponian Herder
Large Munsterlander
Leonberger
Lhasa Apso
Lhasapoo
Long-Haired Rottweiler
Lowchen
Lurcher
Malchi
Malteagle
Maltese
Manchester Terrier
Maremma Sheepdog
Mastador
Mastiff Mix
Mauzer
Meagle
Miki
Mini Labradoodle
Miniature Bull Terrier
Miniature Husky
Miniature Pinscher
Mongrel
Morkie
Moscow Watchdog
Mountain Feist
Mudi
Neapolitan Mastiff
Newfypoo
Norfolk Terrier
Norrbottenspets
Northern Inuit Dog
Norwegian Buhund
Norwegian Elkhound
Norwegian Lundehund
Norwich Terrier
Old English Sheepdog
Otterhound
Papillon
Parson Russell Terrier
Parti Schnauzer
Patterdale Terrier
Peagle
Pekingese
Perro De Presa Canario
Peruvian Inca Orchid
Petite Goldendoodle
Piebald Dachshund
Pit Bull
Pitador
Pitsky
Plott Hound Mix
Pocket Beagle
Pocket Pitbull
Pointer
Pointer Mix
Polish Lowland Sheepdog
Pomchi
Pomeagle
Pomsky
Poochon
Poodle
Poogle
Pudelpointer
Pug
Pug Mix
Puggle
Pugshire
Puli
Pumi
Pyrador
Pyredoodle
Pyrenean Mastiff
Raggle
Rat Terrier
Red Nose Pit Bull
Redbone Coonhound
Rhodesian Ridgeback
Rottsky
Rottweiler
Rottweiler Mix
Russell Terrier
Russian Bear Dog
Saarloos Wolfdog
Sable Black German Shepherd
Sable German Shepherd
Saint Bernard
Saint Shepherd
Saluki
Samoyed
Sapsali
Sarplaninac
Schapendoes
Schipperke
Schneagle
Scotch Collie
Scottish Deerhound
Scottish Terrier
Sealyham Terrier
Shepadoodle
Shepkita
Shepweiler
Shiba Inu
Shichi
Shih Poo
Shih Tzu
Shikoku
Shiranian
Siberian Husky
Siberian Retriever
Siberpoo
Silken Windhound
Silky Terrier
Silver Labrador
Skye Terrier
Smooth Fox Terrier
Spanador
Spanish Mastiff
Spinone Italiano
Springador
Springerdoodle
Stabyhoun
Staffordshire Bull Terrier
Standard Schnauzer
Swedish Lapphund
Swedish Vallhund
Taco Terrier
Tamaskan
Teacup Chihuahua
Teacup Maltese
Teacup Poodle
Teddy Roosevelt Terrier
Tenterfield Terrier
Terrier
Thai Ridgeback
Tibetan Mastiff
Tibetan Spaniel
Tibetan Terrier
Tornjak
Tosa
Toy Fox Terrier
Transylvanian Hound
Treeing Tennessee Brindle
Treeing Walker Coonhound
Utonagan
Vizsla
Volpino Italiano
Weimaraner
Weimardoodle
Welsh Corgi
Welsh Terrier
West Highland Terrier
Wheaten Terrier
Whippet
Whoodle
Wire Fox Terrier
Wirehaired Pointing Griffon
Wirehaired Vizsla
Wolf
Xoloitzcuintli
Yakutian Laika
Yorkie Bichon
Yorkshire Terrier
Zuchon