bckg = [
    "C8E4B2",
    "9ED2BE",
    "7EAA92",
    "FFD9B7",
    "FFC6AC",
    "FFF6DC",
    "C4C1A4",
    "9E9FA5",
    "9BABB8",
    "A1CCD1",
    "FF9B9B",
    "8294C4",
    "DBC4F0",
    "FDFFAE",
    "867070",
    "6096B4",
    "93BFCF",
    "BDCDD6",
    "B9F3FC",
    "BA94D1",
    "FFE1E1",
    "D3CEDF",
]

colors = [
    "Black",
    "Navy",
    "DarkBlue",
    "MediumBlue",
    "Blue",
    "DarkGreen",
    "Green",
    "Teal",
    "DarkCyan",
    "DeepSkyBlue",
    "DarkTurquoise",
    "MediumSpringGreen",
    "Lime",
    "SpringGreen",
    "Aqua",
    "Cyan",
    "MidnightBlue",
    "DodgerBlue",
    "LightSeaGreen",
    "ForestGreen",
    "SeaGreen",
    "DarkSlateGray",
    "DarkSlateGrey",
    "LimeGreen",
    "MediumSeaGreen",
    "Turquoise",
    "RoyalBlue",
    "SteelBlue",
    "DarkSlateBlue",
    "MediumTurquoise",
    "Indigo",
    "DarkOliveGreen",
    "CadetBlue",
    "CornflowerBlue",
    "RebeccaPurple",
    "MediumAquaMarine",
    "DimGray",
    "DimGrey",
    "SlateBlue",
    "OliveDrab",
    "SlateGray",
    "SlateGrey",
    "LightSlateGray",
    "LightSlateGrey",
    "MediumSlateBlue",
    "LawnGreen",
    "Chartreuse",
    "Aquamarine",
    "Maroon",
    "Purple",
    "Olive",
    "Gray",
    "Grey",
    "SkyBlue",
    "LightSkyBlue",
    "BlueViolet",
    "DarkRed",
    "DarkMagenta",
    "SaddleBrown",
    "DarkSeaGreen",
    "LightGreen",
    "MediumPurple",
    "DarkViolet",
    "PaleGreen",
    "DarkOrchid",
    "YellowGreen",
    "Sienna",
    "Brown",
    "DarkGray",
    "DarkGrey",
    "LightBlue",
    "GreenYellow",
    "PaleTurquoise",
    "LightSteelBlue",
    "PowderBlue",
    "FireBrick",
    "DarkGoldenRod",
    "MediumOrchid",
    "RosyBrown",
    "DarkKhaki",
    "Silver",
    "MediumVioletRed",
    "IndianRed",
    "Peru",
    "Chocolate",
    "Tan",
    "LightGray",
    "LightGrey",
    "Thistle",
    "Orchid",
    "GoldenRod",
    "PaleVioletRed",
    "Crimson",
    "Gainsboro",
    "Plum",
    "BurlyWood",
    "LightCyan",
    "Lavender",
    "DarkSalmon",
    "Violet",
    "PaleGoldenRod",
    "LightCoral",
    "Khaki",
    "AliceBlue",
    "HoneyDew",
    "Azure",
    "SandyBrown",
    "Wheat",
    "Beige",
    "WhiteSmoke",
    "MintCream",
    "GhostWhite",
    "Salmon",
    "AntiqueWhite",
    "Linen",
    "LightGoldenRodYellow",
    "OldLace",
    "Red",
    "Fuchsia",
    "Magenta",
    "DeepPink",
    "OrangeRed",
    "Tomato",
    "HotPink",
    "Coral",
    "DarkOrange",
    "LightSalmon",
    "Orange",
    "LightPink",
    "Pink",
    "Gold",
    "PeachPuff",
    "NavajoWhite",
    "Moccasin",
    "Bisque",
    "MistyRose",
    "BlanchedAlmond",
    "PapayaWhip",
    "LavenderBlush",
    "SeaShell",
    "Cornsilk",
    "LemonChiffon",
    "FloralWhite",
    "Snow",
    "Yellow",
    "LightYellow",
    "Ivory",
    "White"
]

seasons = [
    "spring",
    "summer",
    "autumn",
    "winter",
]

numbers_of_friends = [
    "betwen 1 and 5",
    "betwen 6 and 10",
    "betwen 10 and 20",
    "betwen 20 and 50",
    "more than 50",
]

relationship_status = [
    "single",
    "engaged",
    "married",
    "in an open relationship",
    "separated",
    "divorced",
    "in a civil union",
    "in a domestic partnership",
]

heights = [
    "short",
    "normal",
    "tall",
]

weights = [
    "skinny",
    "normal",
    "overweight",
]

eyes_color = [
    "Red",
    "HotPink",
    "DarkOrange",
    "MediumOrchid",
    "RebeccaPurple",
    "Indigo",
    "LimeGreen",
    "DarkGreen",
    "Cyan",
    "Blue",
    "Navy",
    "Goldenrod",
    "SaddleBrown",
    "DarkGray", 
    "Other"
]

skin_color = [
    "Lightpink",
    "Darkorange",
    "Peachpuff",
    "Tan",
    "Other"
]
