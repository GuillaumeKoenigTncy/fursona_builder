# FROM python:3.7-alpine
# WORKDIR /code
# ENV FLASK_APP=app.py
# ENV FLASK_RUN_HOST=0.0.0.0
# RUN apk add --no-cache gcc musl-dev linux-headers python3-dev
# COPY ./app/requirements.txt requirements.txt
# RUN pip3 install -r requirements.txt
# EXPOSE 5000
# COPY . .
# CMD ["flask", "run"]


############################################################
# Dockerfile to build Flask App
# Based on
############################################################

# Set the base image
FROM debian:bullseye

WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0

# File Author / Maintainer
LABEL KOENIG Guillaume

RUN apt-get update && apt-get install -y \
    build-essential \
    python \
    python-dev\
    python3-pip \
    vim \
    libexpat1 \
 && apt-get clean \
 && apt-get autoremove \
 && rm -rf /var/lib/apt/lists/*

# Copy over and install the requirements
COPY ./app/requirements.txt requirements.txt
RUN pip install -r requirements.txt

EXPOSE 5000
COPY . .
CMD ["flask", "run"]